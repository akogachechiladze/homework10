package com.example.usaidhomework10061021

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.ImageView
import androidx.viewpager2.widget.ViewPager2
import com.example.usaidhomework10061021.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.items_layout.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    val items = listOf<Items>(
        Items(R.mipmap.cxeli1, "ოპერაცია - პაუკა", "გენო შავდია - 'დაყარეთ იარაღი და გამოდით, ნიჟარაძე უკან'"),
        Items(R.mipmap.cxeli2, "სასაფლაოს მოჩვენება" , "გლობუსა კალდუნას უეცარმა გამოჩენამ დააფრთხო"),
        Items(R.mipmap.cxeli3, "ბუტკის პატრონი" , "ბონდოს გატისკვამდე დარჩენილია 2 წუთი"),
        Items(R.mipmap.cxeli4, "სტუმრად ომარასთან", "ზახარიჩი ჩრდილში უფრო ლამაზია"),
        Items(R.mipmap.cxeli5, "ბორის ჯოტოს ძე გარუჩავა", "'არც არაფერს ვადასტურებ და არც არაფერს ვუარყოფ'"),
        Items(R.mipmap.cxeli6, "გამომძიებელი ედემი ღურწკაია, რაჟდენიჩი", "ნამდვილი გამომძიებელი სიმართლის დასადგენად ყველაფერს იკადრებს"),
        Items(R.mipmap.cxeli7, "გურამა ჯინორია", "-რუსეთი საქართველოს გარეშე ვერ იარსებებს "),
        )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewpager.adapter = PhotosAdapter(items)

        init()

    }

    private fun init() {
        viewpager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                textview.setText((position + 1).toString())
                textviewimagename.setText(items[position].name.toString())
                textviewdescription.setText(items[position].description.toString())
            }
        })
    }

}