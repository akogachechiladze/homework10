package com.example.usaidhomework10061021

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.items_layout.view.*


class PhotosAdapter(private var items: List<Items>): RecyclerView.Adapter<PhotosAdapter.PhotosViewHolder> () {



    inner class PhotosViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        constructor(parent: ViewGroup) : this(LayoutInflater.from(parent.context).inflate(R.layout.items_layout, parent, false))


        fun bind(items: Items ) {

            itemView.imageview.setImageResource(items.image)

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotosViewHolder {
        return PhotosViewHolder(parent)
    }

    override fun onBindViewHolder(holder: PhotosViewHolder, position: Int) {
        holder.bind(items[position])
    }


    override fun getItemCount() = items.size


}


